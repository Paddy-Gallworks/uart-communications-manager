#include <string.h>
#include <Arduino.h>
#include <base64.hpp>

#ifndef COMMUNUCATION_MANAGER_H
#define COMMUNICATION_MANAGER_H

// ----------------------------------------
// COMMUNICATION MANAGER CONFIGURATION
// ----------------------------------------

#define SOFTWARE_VERSION "0.01"

// ALPHABET VERSION
#define INSTRUCTION_SET_SIZE 26 //lower case alphabet only
// UTILITY FAILURE CODES
#define COMMAND_NOT_RECOGNIZED 48 // ASCII 0
#define COMMAND_DEFINITION_OUT_OF_BOUNDS 49 // ASCII 1
#define SECOND_BYTE_TIME_OUT 50 // ASCII 2
#define TRIPLICATE_MISMATCH 51 // ASCII 3
// COMMUNICATION COSTANTS
const char acknowledge = 6; //ASCII Value for ACK
const char negative_acknowledge = 21; //ASCII Value for NAK
const char line_feed = 10;
const char carridge_feed = 13;
// MODE CONSTATS
#define VERBOSE_MODE 0
#define PACKET_LENGTH 96
#define ENCODED_PACKET_LENGTH 128

struct CommunicationManagerResponse {
  char command;
  String message;
  int errorCode;
};

class CommunicationManager {
//data
public:
protected:
  void (*cmdFunc[INSTRUCTION_SET_SIZE])(char);
  char iden[INSTRUCTION_SET_SIZE];
  uint8_t npara[INSTRUCTION_SET_SIZE];
  byte buff[3]; //Triplicate buffer
  byte que[2];
  uint8_t que_index = 0;
  //bool command_recognized;
  //bool command_carry_over;
  int8_t index_of_command; //TODO get rid of this?
  byte carryOverCmd;
  uint16_t reset_timer = 0;
  bool timeResetMode = false;
  uint16_t reset_time_limit = 10000;
  // Operating Modes
  bool verbose = false;
  bool b64Mode = false;
public:
// Constructor
  CommunicationManager(); //
  CommunicationManager(bool, bool); //Setup with modes
// Functions
  CommunicationManagerResponse check(); //
  void addCommand(uint8_t,char, uint8_t, void command(char)); //
  void setVerbose(bool);
  void setTimeResetMode(bool); // TODO: Not yet implemented
  void setResetTimeOut(uint16_t);
  void setB64Mode(bool); //Change Data Modes
  void transmitDataSerial(Stream &, char *, uint16_t); //Transmit Data Generic Function
  void sendErrorCode(char _buff, byte errorCode);
  void sendAcknowledgeMessage(char, String);
protected:
  int getCommandIndex(char);
  CommunicationManagerResponse checkQue();
  void callWithReset(int, char _arg);
  void resetState(); // Clears all que and buffers
  void checkTimeOut();
  void clearTimeOut();
  //Protected Data Transmit Functions
  void transmitThroughMode(Stream &, char *, uint16_t); // Send data straight onto serial bus.
  void transmit64Encode(Stream &, char *, uint16_t); //Send a b64 encoded string onto the serial port.
};

#endif // COMMUNICATION_MANAGER_H

#include "CommunicationManager.cpp" //Required to work around arduino-cli limitation
