# UART Communications Manager

A library supporting command and data transfer between agency and operator micro controllers using the hardware UART ports.



## Known Limitations:

- Only the 'Serial' port (UART) of the Arduino is considered. This cannot be changed to any of the other serial ports on the Arduino (UART1, UART2 etc.)
