//#include "CommunicationManager.h"

//
// - CommunicationManager Object Constructor
//

CommunicationManager::CommunicationManager() {
  for(int index = 0; index++; index < INSTRUCTION_SET_SIZE) {
    iden[index] = ' ';
  }
}

CommunicationManager::CommunicationManager(bool _verbose, bool _b64Mode) {
  verbose = _verbose;
  b64Mode = _b64Mode;
  CommunicationManager();
}

//
// - check() - Check Communications with OBC
//
//
//
//
CommunicationManagerResponse CommunicationManager::check() {
  while(Serial.peek() == line_feed || Serial.peek() == carridge_feed) {
    Serial.read();
  }
  if(Serial.available() > 0) {
    clearTimeOut();
    buff[2] = buff[1];
    buff[1] = buff[0];
    buff[0] = Serial.read();
    if(verbose) {
      Serial.println();
      Serial.print("Buffer Contents: ");
      Serial.print((char)buff[0]);
      Serial.print(" - ");
      Serial.print((char)buff[1]);
      Serial.print(" - ");
      Serial.println((char)buff[2]);
    }
    if (buff[0]==buff[1] && buff[0]==buff[2]) { //TODO Implement Voting here?
      que[que_index] = buff[0];
      que_index++;
      return checkQue();
    }
    else { // The buffers do not match
      CommunicationManagerResponse commErr;
      commErr.command = buff[0];
      commErr.message = "Triplicate Mismatch";
      commErr.errorCode = TRIPLICATE_MISMATCH;
      return commErr;
    } // end of: else { // The buffers do not match
  } //end of: if(Serial.available() > 0) {
  checkTimeOut();
  CommunicationManagerResponse commErr;
  commErr.command = buff[0];
  commErr.message = "Nominal";
  commErr.errorCode = -1;
  return commErr;
}

void CommunicationManager::addCommand(uint8_t _index, char _iden, uint8_t _npara, void command(char)) {
  iden[_index] = _iden;
  npara[_index] = _npara;
  cmdFunc[_index] = command;
}
void CommunicationManager::setVerbose(bool _verbose) {
    verbose = _verbose;
}
void CommunicationManager::setTimeResetMode(bool _timeResetMode) {
  timeResetMode = _timeResetMode;
}
void CommunicationManager::setResetTimeOut(uint16_t _maxTime) {
  reset_time_limit = _maxTime;
}

void CommunicationManager::setB64Mode(bool _b64Mode) {
  b64Mode = _b64Mode;
}

void CommunicationManager::transmitDataSerial(Stream &serialPort, char *_buff, uint16_t _length) {
  if(b64Mode) {
    transmit64Encode(serialPort,_buff,_length);
  }
  else {
    transmitThroughMode(serialPort,_buff,_length);
  }
}
void CommunicationManager::sendErrorCode(char _buff, byte errorCode) {
    if(verbose) {
        Serial.print("Error: ");
        Serial.println(errorCode);
    }
    else {
        Serial.print(negative_acknowledge);
        Serial.write(errorCode);
    }
}

// -----------------------------------------------------------------------------
// PROTECTED FUNCTIONS
// -----------------------------------------------------------------------------

void CommunicationManager::sendAcknowledgeMessage(char _buff, String _message) {
  if(verbose) {
    Serial.print(_message);
    Serial.write(_buff);
    Serial.println();
  }
  else {
    Serial.write(_buff);
    Serial.print(acknowledge);
  }
}
int CommunicationManager::getCommandIndex(char _buff){
  for(int index = 0; index < INSTRUCTION_SET_SIZE; index++) {
    if(_buff == iden[index]) {
      return index;
    }
  }
  return -1;
}
CommunicationManagerResponse CommunicationManager::checkQue() {
  int8_t index = getCommandIndex(que[0]);
  if(index >= 0) {
    if(npara[index] == 0) {
      sendAcknowledgeMessage(iden[index], "Single Byte Command Recognized: ");
      callWithReset(index,' ');
    }
    else if(npara[index] == 1) {
      if(que_index ==2) {
        sendAcknowledgeMessage(iden[index], "Two Byte Command Recognized: ");
        callWithReset(index,(char)que[1]);
      }
    }
    else{
      CommunicationManagerResponse commErr;
      commErr.command = (char)que[0];
      commErr.errorCode = COMMAND_DEFINITION_OUT_OF_BOUNDS;
      commErr.message = "Command Parameters Out of Bounds";
      sendErrorCode(iden[index], COMMAND_DEFINITION_OUT_OF_BOUNDS);
      resetState();
      return commErr;
    }
  }
  else { // index < 0
    sendErrorCode(que[0], COMMAND_NOT_RECOGNIZED);
    resetState();
    CommunicationManagerResponse commErr;
    commErr.command = (char)que[0];
    commErr.errorCode = COMMAND_NOT_RECOGNIZED;
    commErr.message = "Command Not Recognized!";
    sendErrorCode(iden[index], COMMAND_DEFINITION_OUT_OF_BOUNDS);
    return commErr;
  }
  CommunicationManagerResponse commErr;
  commErr.command = (char)que[0];
  commErr.errorCode = -1;
  commErr.message = "Nominal";
  return commErr;
}
void CommunicationManager::callWithReset(int index, char _arg) {
  cmdFunc[index](_arg);
  resetState();
}
void CommunicationManager::resetState() {
  index_of_command = -1;
  que_index = 0;
  //command_recognized = false;
  //command_carry_over =false;
  reset_timer = 0;
  que[0] = 0;
  que[1] = 0;
  buff[0]=0;
  buff[1]=0;
  buff[2]=0;
}
void CommunicationManager::checkTimeOut() {
  if(reset_timer > reset_time_limit) {
    resetState();
  }
  reset_timer++;
}
void CommunicationManager::clearTimeOut() {
  reset_timer = 0;
}
//Protected Data Transmit FUNCTIONS
void CommunicationManager::transmitThroughMode(Stream &serialPort, char *_buff, uint16_t _length) {
  serialPort.print(_buff);
  //or!
  /*
  for(int i = 0; i < _length, i++) {serialPort.print(_bufff[i]);}
  */
}
void CommunicationManager::transmit64Encode(Stream &serialPort, char *_buff, uint16_t _length) {
  //Create Empty Arrays inside scope
  unsigned char dataArray[PACKET_LENGTH];
  unsigned char encodedArray[ENCODED_PACKET_LENGTH];
  //Fill Padding and then over write with valid data.
  for(int i = 0; i < PACKET_LENGTH; i++) {dataArray[i] = ' ';} // Fill in padding
  for(int i = 0; i < min(PACKET_LENGTH,_length); i++) {dataArray[i] = _buff[i];}
  //Encode data and write to serial
  for(int i = 0; i < ENCODED_PACKET_LENGTH; i++) {encodedArray[i] = '=';}
  unsigned int m = encode_base64(dataArray, min(PACKET_LENGTH,_length), encodedArray);
  // for(int i = 0; i < ENCODED_PACKET_LENGTH; i++) {
  //   serialPort.write(encodedArray[i]);
  // }
  serialPort.write((char *) encodedArray, 128);
  //If there is remaining data, call this function again with remaining buffer
  if(_length > PACKET_LENGTH) {
    transmit64Encode(serialPort, &_buff[PACKET_LENGTH],_length - PACKET_LENGTH); // Recursively call this function with shorter buffer.
  }
}
